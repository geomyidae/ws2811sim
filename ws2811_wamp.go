// Copyright 2016 Jacques Supcik / HEIA-FR
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ws2811sim

import (
	"fmt"
	"time"

	"gopkg.in/jcelliott/turnpike.v2"
)

const (
	WampTopic = "frame"
)

type WS2811Sim struct {
	buffer     []uint32
	wampClient *turnpike.Client
	timestamp  time.Time
	frequency  uint32
}
type Option struct {
	Frequency  uint32
	LedCount   int
	WampClient *turnpike.Client
}

var DefaultOptions = Option{
	Frequency: 800000,
	LedCount:  16,
}

func MakeWS2811(opt *Option) *WS2811Sim {
	ws := WS2811Sim{
		frequency:  opt.Frequency,
		wampClient: opt.WampClient,
		buffer:     make([]uint32, opt.LedCount),
	}
	return &ws
}

func (ws *WS2811Sim) Init() error {
	return nil
}

func (ws *WS2811Sim) Render() error {
	ws.Wait() // Make sure that this statement comes before changing the timestamp.
	ws.timestamp = time.Now()
	frame := make([]string, len(ws.buffer))
	for i, v := range ws.buffer {
		frame[i] = fmt.Sprintf("%06X", v)
	}
	ws.wampClient.Publish(WampTopic, nil, []interface{}{frame}, nil)
	return nil
}

func (ws *WS2811Sim) Wait() error {
	dt := 1000000.0/float32(ws.frequency)*float32(24*len(ws.buffer)) + 50.0
	time.Sleep(ws.timestamp.Add(time.Duration(dt) * time.Microsecond).Sub(time.Now()))
	return nil
}

// Fini does nothing and is just here for compatibility with the "real" ws2811.
func (ws *WS2811Sim) Fini() {
}

func (ws *WS2811Sim) SetLed(index int, value uint32) {
	ws.buffer[index] = value
}

func (ws *WS2811Sim) SetBitmap(a []uint32) {
	for i := 0; i < len(ws.buffer) && i < len(a); i++ {
		ws.buffer[i] = a[i]
	}
}

func (ws *WS2811Sim) Clear() {
	for i := 0; i < len(ws.buffer); i++ {
		ws.buffer[i] = 0
	}
}
