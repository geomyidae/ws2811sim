package main

import (
	"log"
	"net/http"

	"gitlab.com/geomyidae/ws2811sim"
	"gopkg.in/jcelliott/turnpike.v2"
)

func main() {
	log.Printf("Hello, world.\n")

	server := turnpike.NewBasicWebsocketServer("ws2811")
	client, err := server.GetLocalClient("ws2811", nil)
	if err != nil {
		log.Panic(err)
	}

	opt := ws2811sim.DefaultOptions
	opt.WampClient = client
	opt.LedCount = 1024
	//opt.Frequency = 1000000
	ws := ws2811sim.MakeWS2811(&opt)

	go func() {
		i := 0
		ws.Clear()
		ws.Render()
		ws.Wait()
		for {
			ws.SetLed(i, 0xff0000+uint32(i))
			ws.Render()
			ws.Wait()
			i = (i + 1) % 1024
		}
	}()

	http.Handle("/data", server)
	http.Handle("/", http.FileServer(FS(false)))
	log.Println("Serving at localhost:8080...")
	log.Fatal(http.ListenAndServe(":8080", nil))
}
