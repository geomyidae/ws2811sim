package main

import (
	"bytes"
	"compress/gzip"
	"encoding/base64"
	"io/ioutil"
	"net/http"
	"os"
	"path"
	"sync"
	"time"
)

type _escLocalFS struct{}

var _escLocal _escLocalFS

type _escStaticFS struct{}

var _escStatic _escStaticFS

type _escDirectory struct {
	fs   http.FileSystem
	name string
}

type _escFile struct {
	compressed string
	size       int64
	modtime    int64
	local      string
	isDir      bool

	once sync.Once
	data []byte
	name string
}

func (_escLocalFS) Open(name string) (http.File, error) {
	f, present := _escData[path.Clean(name)]
	if !present {
		return nil, os.ErrNotExist
	}
	return os.Open(f.local)
}

func (_escStaticFS) prepare(name string) (*_escFile, error) {
	f, present := _escData[path.Clean(name)]
	if !present {
		return nil, os.ErrNotExist
	}
	var err error
	f.once.Do(func() {
		f.name = path.Base(name)
		if f.size == 0 {
			return
		}
		var gr *gzip.Reader
		b64 := base64.NewDecoder(base64.StdEncoding, bytes.NewBufferString(f.compressed))
		gr, err = gzip.NewReader(b64)
		if err != nil {
			return
		}
		f.data, err = ioutil.ReadAll(gr)
	})
	if err != nil {
		return nil, err
	}
	return f, nil
}

func (fs _escStaticFS) Open(name string) (http.File, error) {
	f, err := fs.prepare(name)
	if err != nil {
		return nil, err
	}
	return f.File()
}

func (dir _escDirectory) Open(name string) (http.File, error) {
	return dir.fs.Open(dir.name + name)
}

func (f *_escFile) File() (http.File, error) {
	type httpFile struct {
		*bytes.Reader
		*_escFile
	}
	return &httpFile{
		Reader:   bytes.NewReader(f.data),
		_escFile: f,
	}, nil
}

func (f *_escFile) Close() error {
	return nil
}

func (f *_escFile) Readdir(count int) ([]os.FileInfo, error) {
	return nil, nil
}

func (f *_escFile) Stat() (os.FileInfo, error) {
	return f, nil
}

func (f *_escFile) Name() string {
	return f.name
}

func (f *_escFile) Size() int64 {
	return f.size
}

func (f *_escFile) Mode() os.FileMode {
	return 0
}

func (f *_escFile) ModTime() time.Time {
	return time.Unix(f.modtime, 0)
}

func (f *_escFile) IsDir() bool {
	return f.isDir
}

func (f *_escFile) Sys() interface{} {
	return f
}

// FS returns a http.Filesystem for the embedded assets. If useLocal is true,
// the filesystem's contents are instead used.
func FS(useLocal bool) http.FileSystem {
	if useLocal {
		return _escLocal
	}
	return _escStatic
}

// Dir returns a http.Filesystem for the embedded assets on a given prefix dir.
// If useLocal is true, the filesystem's contents are instead used.
func Dir(useLocal bool, name string) http.FileSystem {
	if useLocal {
		return _escDirectory{fs: _escLocal, name: name}
	}
	return _escDirectory{fs: _escStatic, name: name}
}

// FSByte returns the named file from the embedded assets. If useLocal is
// true, the filesystem's contents are instead used.
func FSByte(useLocal bool, name string) ([]byte, error) {
	if useLocal {
		f, err := _escLocal.Open(name)
		if err != nil {
			return nil, err
		}
		b, err := ioutil.ReadAll(f)
		f.Close()
		return b, err
	}
	f, err := _escStatic.prepare(name)
	if err != nil {
		return nil, err
	}
	return f.data, nil
}

// FSMustByte is the same as FSByte, but panics if name is not present.
func FSMustByte(useLocal bool, name string) []byte {
	b, err := FSByte(useLocal, name)
	if err != nil {
		panic(err)
	}
	return b
}

// FSString is the string version of FSByte.
func FSString(useLocal bool, name string) (string, error) {
	b, err := FSByte(useLocal, name)
	return string(b), err
}

// FSMustString is the string version of FSMustByte.
func FSMustString(useLocal bool, name string) string {
	return string(FSMustByte(useLocal, name))
}

var _escData = map[string]*_escFile{

	"/index.html": {
		local:   "html/index.html",
		size:    913,
		modtime: 1480973484,
		compressed: `
H4sIAAAJbogA/4yTT2/bPAzG7/0UfPXukAK11D/bULR2LtmAHTZswHLpaVAkJlYmS65IO0mHfffBtpL2
smLwwaRI/h7psVz+9+HrYvnw7SPU3Pj5WTm9AMoatR0CgLJB1lAztwU+dq6vxCIGxsDF8tCiADNllWDc
sxoA92BqnQi56nhd3IqXnKAbrETvcNfGxC+md85yXVnsncFiTC7ABcdO+4KM9lhdHUHs2ON8iR5NbGAZ
d5hKNS1ODd6Fn5DQV4L44JFqRBZQJ1xXYjgI3SllbJBbsuhdn2RAVm2XUF3K9/JyDIvGBWmIxD8wFbFm
Z5QhUmP1NFiqo5HlKtpDZlnXg/GaqBL5/FkFoDQ69JrA2Up4tN85uRYX45qAkV2JVUwW0911uweK3ln4
394Mz/2J8hC7BKsUd4QJbESCEBmoawfTgWuET8svn99BFmO9kaWakrxFZV2fQzLJtQyUzLN7eqv3chPj
xqNuHUkTm3FNebcitX3sMB3UtbySb3MiBzu3JOalmnivwTuOK10HSTdSN/opBr3LErmyJeU1I/Fz78jf
PL0qcPxOwzhaGr39+6aOZr6ZgY2mazAwnMuE2h5m6y4YdjHMzuEX5D4AYp34x8SdXV3fXsDt+X2u/s7R
SQrG6zHdilJNP97ZnwAAAP//Xs4RHpEDAAA=
`,
	},

	"/static/css/style.css": {
		local:   "html/static/css/style.css",
		size:    56,
		modtime: 1480892118,
		compressed: `
H4sIAAAJbogA/9JLzs8rSc0rUajmUlBQUMhNLErPzNMtyS+wUjAoqLBGFsxJTSuBitZyAQIAAP//nOU0
+jgAAAA=
`,
	},

	"/static/js/ledstripe.js": {
		local:   "html/static/js/ledstripe.js",
		size:    1998,
		modtime: 1480975643,
		compressed: `
H4sIAAAJbogA/6xVwW7bOBC96yseXBSmbUWR1YshVZcNdoEeChTIMQgKWqIlojJlkFQsofC/L0iKspxs
scFucwg4nOcZznuP4guVaFiptOQnhhw/A0DSkncqRRIGwJHKiosUn0yg2CnF1qyKtumOQqXYJjsTy/as
UtjlUVWPmhY/Ujw9W2gnJRP6L0mPLEU8bjGhvzKlaMVS/LyEQQBwwXWKQycKzVtBVvYwgK65is681DVy
F4zNsQaxsTsw1kiwcQjFTivcTWtskGDtQjdQdi1dM17V2tc2g/z/wobWAjnKtuiOTOioYvrPhpnlH8OX
kiwaVj5azh+oeKFqsXK/K24HtYHP3J7TRbNuukeOwjR6aIVmvSaLpDR13c91H+1ZxcU3qmviu+k+kqzQ
JA7j8NownDWYIQ+8aR710LB88eGT/VvcJolvhkMrQcyphjzOMODzldsMw2bjpZ0he4PsPXJUOEM/B/9i
DJ+gsiDTBkYJX4nkFF1hjd5L6nTzkQOE768zvLfOfBtxiGT9leo6+vbl1RhKy/YHG5k+11yzxT8ByCqz
O2PqEvj/7jJ1p5Jq9r2w9prdqpJq6hn9ry59h98mXTlyxBk4PsO0jhomKl1n4HNhrVWQg+PjzCmzpOlF
OO4wrHD/FsIPID0+IkGeI54bZri513fYmhrZDWe/dNUbT/0eR/0eP/27mwxvstobfammT/x5Ptd0mZFj
+WGJjYG+BngqvKsuWRB4I0FpKvV393CQ8b6G9h1w/E+vyvS5zv2rkd2krTI5vJ7XhHkPxm+KdVwrBHO9
cwh2Bu10u6e1iB6mDHHKd7JJsTyr9P6+aQva1K3S6S7exfeGiqVjUTLaHC0s2W23SzOja3ZtFLWiPTHT
bxqbKKYUb4U32ZRoBXthQhMqK3V14HWamwtpUU/x84xg87raypHq9qqQfM/I8mAezWXoi1v45c0hT0wY
ni7B3wEAAP//jwo9V84HAAA=
`,
	},

	"/": {
		isDir: true,
		local: "html",
	},

	"/static": {
		isDir: true,
		local: "html/static",
	},

	"/static/css": {
		isDir: true,
		local: "html/static/css",
	},

	"/static/js": {
		isDir: true,
		local: "html/static/js",
	},
}
